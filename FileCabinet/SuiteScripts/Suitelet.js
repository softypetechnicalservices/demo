/**
 * @NApiVersion 2.1
 * @NScriptType Suitelet
 * @NModuleScope SameAccount
 */
define(['N/xml', 'N/render', 'N/http'],

function(xml, render, http) {
   
   
    const onRequest = (context) => {
    if(context.request.method == 'GET'){
    	let info = {
    			name: "Ren",
    			address: "Cebu City"
    	}
    	let htmlvar =`Hi I'm ${info.name} from ${info.address}`;
    	let obj = new Object();
		let file = render.xmlToPdf
		({
			xmlString: htmlvar
		});

		context.response.writeFile(file, true);
		return;
    }
    }

    return {
        onRequest: onRequest
    };
    
});
